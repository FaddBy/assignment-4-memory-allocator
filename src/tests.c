#define _DEFAULT_SOURCE

#include <stdio.h>

#include "tests.h"
#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <sys/mman.h>


#define INIT_HEAP_SIZE 80000

static struct block_header* get_block_by_address(void* data) {
    return (struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}

void* map_pages(void const* addr, size_t length) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0 );
}


void test_regular_data_allocate(struct block_header* heap) {
    printf("TEST 1: regular_data_allocate\n");

    const size_t size = 500;
    void *data = _malloc(size);

    if (data == NULL)
        err("FAILED TO ALLOCATE MEMORY, DATA IS NULL\n");

    debug_heap(stdout, heap);

    if (heap->is_free)
        err("ALLOCATED BLOCK IS FREE\n");

    if (heap->capacity.bytes != size)
        err("ALLOCATED BLOCK HAS WRONG CAPACITY\n");

    printf("TEST 1: PASSED\n");

    _free(data);
}

void test_allocation_one_from_allocated(struct block_header* heap) {
    printf("TEST 2: test_allocation_one_from_allocated\n");

    void *data1 = _malloc(500);
    void *data2 = _malloc(700);

    if (data1 == NULL || data2 == NULL)
        err("FAILED TO ALLOCATE BLOCKS");

    _free(data1);

    debug_heap(stdout, heap);

    struct block_header *block1 = get_block_by_address(data1);
    struct block_header *block2 = get_block_by_address(data2);

    if (!block1->is_free) {
        err("FAILED TO FREE FIRST BLOCK\n");
    }

    if (block2->is_free) {
        err("FAILED TO ALLOCATE SECOND BLOCK\n");
    }

    printf("TEST 2: PASSED\n");

    _free(data1);
    _free(data2);
}

void test_allocation_two_from_allocated(struct block_header* heap) {
    printf("TEST 3: test_allocation_two_from_allocated\n");

    const size_t size1 = 500, size2 = 1000, size3 = 1500;

    void *data1 = _malloc(size1);
    void *data2 = _malloc(size2);
    void *data3 = _malloc(size3);

    if (data1 == NULL || data2 == NULL || data3 == NULL) {
        err("FAILED TO ALLOCATE BLOCKS\n");
    }

    _free(data2);
    _free(data1);

    debug_heap(stdout, heap);

    struct block_header *block1 = get_block_by_address(data1);
    struct block_header *block3 = get_block_by_address(data3);

    if (!block1->is_free) {
        err("FAILED TO FREE FIRST BLOCK\n");
    }

    if (block3->is_free) {
        err("FAILED TO ALLOCATE THIRD BLOCK\n");
    }


    if (block1->capacity.bytes != size1 + size2 + offsetof(struct block_header, contents)) {
        err("FAILED TO MERGE BLOCKS\n");
    }

    printf("TEST 3: PASSED\n");

    _free(data3);
    _free(data1);
    _free(data2);
}

void test_new_region(struct block_header *heap) {
    printf("TEST 4: test_new_region\n");

    void *data1 = _malloc(INIT_HEAP_SIZE);
    void *data2 = _malloc(INIT_HEAP_SIZE + 1000);
    void *data3 = _malloc(2000);

    if (data1 == NULL || data2 == NULL || data3 == NULL) {
        err("FAILED TO ALLOCATE BLOCKS\n");
    }

    _free(data3);
    _free(data2);

    debug_heap(stdout, heap);

    struct block_header *block1 = get_block_by_address(data1);
    struct block_header *block2 = get_block_by_address(data2);

    if ((uint8_t *)block1->contents + block1->capacity.bytes != (uint8_t*) block2) {
        err("FAILED TO ALLOCATE REGION\n");
    }

    _free(data1);
    _free(data2);
    _free(data3);
    printf("TEST 4: PASSED \n");

    
}

void test_new_region_in_new_place(struct block_header *heap) {
    printf("TEST 5: test_new_region_in_new_place\n");

    void *data1 = _malloc(150000);
    if (data1 == NULL) {
        err("FAILED TO ALLOCATE BLOCK\n");
    }

    struct block_header *address = heap;
    while (address->next != NULL) {
        address = address->next;
    }
    map_pages((uint8_t*) address + size_from_capacity(address->capacity).bytes, 1024);

    void *data2 = _malloc(100000);

    debug_heap(stdout, heap);

    struct block_header *block2 = get_block_by_address(data2);

    if (block2 == address) {
        err("FAILED TO ALLOCATE REGION IN NEW PLACE\n");
    }

    printf("TEST 5: PASSED\n");

    _free(data1);
    _free(data2);
}


void all_tests(void) {
    printf("START TESTING\n");
    struct block_header* heap = (struct block_header*) heap_init(INIT_HEAP_SIZE);
    if (heap == NULL || heap->capacity.bytes < INIT_HEAP_SIZE) {
        err("FAILED TO INITIALIZE HEAP\n");
    }

    test_regular_data_allocate(heap);
    test_allocation_one_from_allocated(heap);
    test_allocation_two_from_allocated(heap);
    test_new_region(heap);
    test_new_region_in_new_place(heap);

    printf("ALL TESTS: PASSED\n");
}
